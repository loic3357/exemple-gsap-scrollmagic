import VueRouter from "vue-router";

import Accueil from "../pages/Accueil";


const routes = [
    {path: '/', name: 'accueil', component: Accueil}
];

const router = new VueRouter({
    mode: 'history',
    routes
});

export default router;
